// routes.js
const express = require('express');
const axios = require('axios');
const { externalUrl } = require('../config/config'); // Import the external URL from config.js
const asyncHandler = require('../utils/async'); // Import the asyncHandler

const router = express.Router();

router.get(
    '/getextracts',
    asyncHandler(async (req, res) => {
      const response = await axios.get(externalUrl);
      res.json(response.data);
    })
);

module.exports = router;
