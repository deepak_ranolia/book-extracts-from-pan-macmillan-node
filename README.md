# Node Proxy Server

A simple Node.js proxy server to add CORS headers to responses.

## Getting Started

These instructions will help you get the Node Proxy Server up and running on your local machine.

### Prerequisites

Before running the server, ensure you have the following software and dependencies installed:

- [Node.js](https://nodejs.org/): Make sure you have Node.js installed on your system.

### Installation

1. Clone the repository:

   ```git clone https://github.com/yourusername/node-proxy-server.git```

2. Navigate to the project directory:
    ```cd node-proxy-server```

3. Install project dependencies:
    ```npm install```

### Running the server
To start the Node Proxy Server, run the following command:
    ```npm start```

The server will be running at [http://localhost:3000](http://localhost:3000)

### Front-End Code
The front-end code for this project can be found at the following URL:

[Front-End Repository](https://react-book-extracts-pan.web.app/)
[github link](https://github.com/offline-pixel/react-book-extracts-pan)
