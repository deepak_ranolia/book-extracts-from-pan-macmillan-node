// server.js
const express = require('express');
const cors = require('cors');
const routes = require('./routes/routes');
const { corsOptions } = require('./config/config');

const app = express();
const port = process.env.PORT || 3001;

app.use(express.json());

// Enable CORS with your configuration
app.use(cors(corsOptions));

// Use your routes
app.use('/api', routes);

app.listen(port, () => {
  console.log(`Server is running on port ${port}`);
});
