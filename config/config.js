// config.js
module.exports = {
  corsOptions: {
    origin: ['http://localhost:3000', 'https://example.com', 'https://another-domain.com', 'https://react-book-extracts-pan.web.app'],
    methods: 'GET', // You can specify the allowed HTTP methods here
  },
  externalUrl: 'https://extracts.panmacmillan.com/getextracts?titlecontains=s',
};